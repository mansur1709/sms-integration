<div id="main">
    <div class="container">
        <div class="logo">
            <img src="<?php echo base_url("media/images/logo.png"); ?>" alt="" />
        </div>    
        <div class="row">
            <div class="col-md-4">                
                <div class="text bg">
                    <?php echo $this->session->flashdata('save_response'); ?>
                    <div>
                        <button class="open-copy-paste my-btn pull-left">Copy Paste</button>
                        <button class="open-spreadsheet my-btn pull-right">Upload Spreadsheet</button>
                        <div class="clearfix"></div>
                    </div>
                    <div class="copy-paste-upload">
                        <h2>Copy, Paste and Mobile Numbers</h2>
                        <form action="<?php echo base_url("index.php/save_numbers/save_numbers_action"); ?>" method="post" accept-charset="utf-8">
                            <p><strong>It's as easy as 1, 2, 3!</strong></p>
                            <ol>
                                <li>Copy list of numbers to save from wherever possible (Make sure mobile numbers are aligned as one per line)</li>
                                <li>Paste them in the below text box</li>
                                <li>Now click "Save Numbers Button" </li>
                            </ol>
                            <p><textarea class="c-p" name="numbers" id="numbers"  onKeyPress="validate(event)"><?php echo (isset($mobileNumbers)?$mobileNumbers:""); ?></textarea></p>
                            <p><input name="ac" id="ac" type="hidden" value="copy_paste"></p>
                            <ul class="contact">
                                <li><input type="radio" name="contact_type" id="contact_1" value="contact_1" checked /><label class="radio-contact" for="contact_1">Carpenter</label></li>
                                <li><input type="radio" name="contact_type" id="contact_2" value="contact_2" /><label class="radio-contact" for="contact_2">Engineer</label></li>
                            </ul>
                            <input type="submit" name="copy_paste" value="Save Numbers" class="btn btn-checkout btn-cart btn-default btn-sm" onclick="$('#loader').show();">
                        </form>
                    </div>
                    <div class="spreadsheet-upload">
                        <h2>Upload Spreadsheet </h2>
                        <form action="<?php echo base_url("index.php/save_numbers/save_numbers_action"); ?>" id="frmUploadSpreadsheet" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                            <p>You can upload a spreadsheet full of Mobile numbers.<br>Select your file below and click 'Upload File'.<br><br>
                            <input name="datafile" type="file" id="datafile"><br>
                            <span>Note:</span>
                            <ul>
                                <li>The file should be in the format of .xls or .csv with only two columns (Mobile Number)</li>
                                <li>Download sample <a href="<?php echo base_url("media/files/sample_numbers.xls"); ?>">.xls file</a> or <a href="<?php echo base_url("media/files/sample_numbers.csv"); ?>">.csv file</a> for the format we accept
                                </li>
                            </ul>
                            <p>
                            <input name="ac" id="ac" type="hidden" value="spreadsheet">
                            <ul class="contact">
                                <li><input type="radio" name="contact_type" id="contact_2_1" value="contact_1" checked /><label class="radio-contact" for="contact_2_1"> Carpenter</label></li>
                                <li><input type="radio" name="contact_type" id="contact_2_2" value="contact_2" /><label class="radio-contact" for="contact_2_2"> Engineer</label></li>
                            </ul>    
                            </p><input type="submit" class="btn btn-checkout btn-cart btn-default btn-sm" onclick="$('#loader').show();">
                        </form>
                    </div>
                    <div class="clearer"></div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="bg">
                    <div class="row">
                        <?php echo $this->session->flashdata('send_response'); ?>
                        <div class="col-md-12 column-two">
                            <div>
                                <div class="remaining-sms-count pull-left">
                                    Remaining SMS count: <b class="sms_count"><?php echo (isset($Text_Count)?$Text_Count:"0"); ?></b><br>
                                    Remaining Voice SMS count: <b class="voice_sms_count"><?php echo (isset($voice_Count)?$voice_Count:"0"); ?></b><br><br>
                                    SMS Sent Today: <b class="voice_sms_count"><?php echo (isset($getSMS)?$getSMS:"0"); ?></b>
                                </div>
                                <div class="remaining-voice-sms-count pull-right">
                                    Total Carpenter: <b class="sms_count"><?php echo (isset($getCountCarpenter)?$getCountCarpenter:"0"); ?></b><br>
                                    Total Engineer: <b class="sms_count"><?php echo (isset($getCountEngineer)?$getCountEngineer:"0"); ?></b><br><br>
                                    Voice SMS Sent Today: <b class="voice_sms_count"><?php echo (isset($getVoice)?$getVoice:"0"); ?></b>
                                </div>
                                <div class="clearfix"></div>
                            </div>     
                            <div>
                                <button class="open-contact-1 my-btn pull-left">Carpenter</button>
                                <button class="open-contact-2 my-btn pull-right">Engineer</button>
                                <div class="clearfix"></div>
                            </div>
                            <div class="contact_1">
                                <form action="<?php echo base_url("index.php/save_numbers/send_SMS"); ?>" name="send-form-1" id="send-form-1" method="post" accept-charset="utf-8">
                                    <input class="search-contact-1" type="text" name="search" placeholder="Search contact one...">
                                    <?php if(!empty($contact_1)) { ?>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <input type="checkbox" id="checkAll-1" /><label for="checkAll-1">Check All (100)</label>
                                            <a href="#">
                                                <label class="pull-right" onClick="return doconfirm('contact_1');" >Delete</label>
                                            </a>
                                            <ul id="mobile-numbers-contact-1">
                                                <?php foreach ($contact_1 as $key => $value) { ?>
                                                    <?php if($value['type'] == 'contact_1') { ?>
                                                        <li><input type="checkbox" class="numbers-cb-contact-1" name="mobile_numbers[]" <?php echo(in_array($value['mobile_numbers'], $mobile_Numbers)) ? 'checked="checked"' : ''?> rel="<?php echo $value['mobile_numbers']; ?>" value="<?php echo $value['mobile_numbers']; ?>" id="num-contact-1-<?php echo $value['mobile_numbers']; ?>"><label for="num-contact-1-<?php echo $value['mobile_numbers']; ?>"><?php echo $value['mobile_numbers']; ?></label></li>
                                                    <?php } ?>    
                                                <?php } ?>            
                                            </ul>
                                        </div>
                                        <div class="col-md-6">
                                            <h4 class="numbers-filter-head">FILTER: <div class="checkbox-count pull-right"></div></h4>
                                            <div class="checked-value-1">
                                                <?php 
                                                if(!empty(array_filter($mobile_Numbers))) { 
                                                    foreach ($mobile_Numbers as $key => $value) {
                                                        echo "<label for='num-contact-1-".$value."' data-cb-id-1='num-contact-1-".$value."'>".$value. "</label>";
                                                    }
                                                } 
                                                ?>
                                            </div>
                                        </div>
                                    </div>    
                                    <?php } else { ?>    
                                        <div class="no-records">No more numbers available.</div>
                                    <?php } ?>
                                    <div>
                                        <div class="text">
                                            <div>
                                                <input type="button" class="open-send-sms my-btn pull-left" value="TEXT SMS" />
                                                <input type="button" class="open-voice-sms my-btn pull-right" value="VOICE SMS" />
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="send-sms">
                                                <h2>Type your message</h2>
                                                <p><textarea class="send-sms" name="send_sms" id="send-sms" onkeyup="countChar(this)"><?php echo (isset($Textmessage)?$Textmessage:""); ?></textarea><div id="charNum"></div></p>
                                                <input type="submit" name="sms" value="send text sms" class="btn btn-checkout btn-cart btn-default btn-sm" onclick="$('#loader').show();">
                                            </div>
                                            <div class="send-voice-sms">
                                               <div class="voice">
                                                    <img src="<?php echo base_url("media/images/voice.png"); ?>" alt="" />
                                                    <h6>Select your voice file</h6>
                                                    <?php 
                                                    if(!empty($voiceFiles)) {
                                                        foreach ($voiceFiles as $key => $value) { ?>
                                                            <input type='radio' name='voice_file' id="contact-2-<?php echo $value['file_id']; ?>" value="<?php echo $value['file_id']; ?>" <?php if($radioSelected == $value['file_id']) { echo 'checked="checked"'; } ?> /><label for="contact-2-<?php echo $value['file_id']; ?>"><?php echo $value['original_name']; ?></label>
                                                        <?php } ?>
                                                    <?php } ?>    
                                                </div>
                                                <input type="hidden" name="type" value="contact_1" />
                                                <input type="submit" name="sms" value="send voice sms" class="btn btn-checkout btn-cart btn-default btn-sm" onclick="$('#loader').show();">
                                            </div>
                                            <div class="clearer"></div>
                                        </div>
                                    </div>
                                </form> 
                            </div>
                            <div class="contact_2">    
                                <form action="<?php echo base_url("index.php/save_numbers/send_SMS"); ?>" name="send-form-2" id="send-form-2" method="post" accept-charset="utf-8">
                                    <input class="search-contact-2" type="text" name="search" placeholder="Search contact two...">
                                    <?php if(!empty($contact_2)) { ?>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="checkbox" id="checkAll-2" /><label for="checkAll-2">Check All (100)</label>
                                                <a href="#">
                                                    <label class="pull-right" onClick="return doconfirm('contact_2');" >Delete</label>
                                                </a>
                                                <ul id="mobile-numbers-contact-2">
                                                    <?php foreach ($contact_2 as $key => $value) { ?>
                                                        <?php if($value['type'] == 'contact_2') { ?>
                                                            <li><input type="checkbox" class="numbers-cb-contact-2" name="mobile_numbers[]" <?php echo(in_array($value['mobile_numbers'], $mobile_Numbers)) ? 'checked="checked"' : ''?> rel="<?php echo $value['mobile_numbers']; ?>" value="<?php echo $value['mobile_numbers']; ?>" id="num-contact-2-<?php echo $value['mobile_numbers']; ?>"><label for="num-contact-2-<?php echo $value['mobile_numbers']; ?>"><?php echo $value['mobile_numbers']; ?></label></li>
                                                        <?php } ?>    
                                                    <?php } ?>            
                                                </ul>
                                            </div>
                                            <div class="col-md-6">
                                                <h4 class="numbers-filter-head">FILTER: <div class="checkbox-count pull-right"></div></h4>
                                                <div class="checked-value-2">
                                                    <?php 
                                                    if(!empty(array_filter($mobile_Numbers))) { 
                                                        foreach ($mobile_Numbers as $key => $value) {
                                                            echo "<label for='num-contact-2-".$value."' data-cb-id-2='num-contact-2-".$value."'>".$value. "</label>";
                                                        }
                                                    } 
                                                    ?>
                                                </div>
                                            </div>
                                        </div>    
                                    <?php } else { ?>    
                                        <div class="no-records">No more numbers available.</div>
                                    <?php } ?>
                                    <div>
                                        <div class="text">
                                            <div>
                                                <input type="button" class="open-send-sms my-btn pull-left" value="TEXT SMS" />
                                                <input type="button" class="open-voice-sms my-btn pull-right" value="VOICE SMS" />
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="send-sms">
                                                <h2>Type your message</h2>
                                                <p><textarea class="send-sms" name="send_sms" id="send-sms" onkeyup="countChar(this)"><?php echo (isset($Textmessage)?$Textmessage:""); ?></textarea><div id="charNum"></div></p>
                                                <input type="hidden" name="type" value="contact_2" />
                                                <input type="submit" name="sms" value="send text sms" class="btn btn-checkout btn-cart btn-default btn-sm" onclick="$('#loader').show();">
                                            </div>
                                            <div class="send-voice-sms">
                                                <div class="voice">
                                                    <img src="<?php echo base_url("media/images/voice.png"); ?>" alt="" />
                                                    <h6>Select your voice file</h6>
                                                    <?php
                                                    if(!empty($voiceFiles)) { 
                                                        foreach ($voiceFiles as $key => $value) { ?>
                                                            <input type='radio' name='voice_file' id="<?php echo $value['file_id']; ?>" value="<?php echo $value['file_id']; ?>" <?php if($radioSelected == $value['file_id']) { echo 'checked="checked"'; } ?> /><label for="<?php echo $value['file_id']; ?>"><?php echo $value['original_name']; ?></label>
                                                        <?php } ?>    
                                                   <?php } ?>
                                                </div>
                                                <input type="hidden" name="type" value="contact_2" />
                                                <input type="submit" name="sms" value="send voice sms" class="btn btn-checkout btn-cart btn-default btn-sm" onclick="$('#loader').show();">
                                            </div>
                                            <div class="clearer"></div>
                                        </div>
                                    </div>
                                </form> 
                            </div>
                        </div>                
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="loader" class="hide"></div> 
<script type="text/javascript">  
    $(window).load(function() {
        $("#loader").removeClass("hide");
        $("#loader").fadeOut("slow");  
   });
</script>
<script>
    $("#send-form-1").submit( function(eventObj) {
        var getNumbers = [];
        $('.checked-value-1 label').each(function(index, val) {
            getNumbers.push($(this).text());
        });
        $('<input />').attr('type', 'hidden')
            .attr('name', "selected_numbers")
            .attr('value', getNumbers.join(","))
            .appendTo('#send-form-1');
        return true;
    }); 
    $("#send-form-2").submit( function(eventObj) {
        var getNumbers = [];
        $('.checked-value-2 label').each(function(index, val) {
            getNumbers.push($(this).text());
        });
        $('<input />').attr('type', 'hidden')
            .attr('name', "selected_numbers")
            .attr('value', getNumbers.join(","))
            .appendTo('#send-form-2');
        return true;
    });   
</script>
<script>
    function validate(e) {
        var keycode = (e.which) ? e.which : e.keyCode;
        var phn = document.getElementById('textarea');
        if ((keycode < 48 || keycode > 57) && keycode !== 13) {
            e.preventDefault();
            console.log("FAIL");
            return false;
        } else {
            console.log("OK!");
        }
    }
</script>
<script>
    $(document).ready(function(){
        $('.spreadsheet-upload').addClass('hide');
        $('.open-copy-paste').addClass("active");
        $('.open-send-sms').addClass("active");
        $('.open-copy-paste').click(function(){
            $('.copy-paste-upload').removeClass("hide");
            $('.spreadsheet-upload').addClass("hide");
            $('.open-copy-paste').addClass("active");
            $('.open-spreadsheet').removeClass("active");
        });

        $('.open-spreadsheet').click(function(){
            $('.copy-paste-upload').addClass("hide");
            $('.spreadsheet-upload').removeClass("hide");
            $('.open-spreadsheet').addClass("active");
            $('.open-copy-paste').removeClass("active");
        });

        $('.send-voice-sms').addClass('hide');
        $('.open-send-sms').click(function(){
            $('.send-sms').removeClass("hide");
            $('.send-voice-sms').addClass("hide");
            $('.open-send-sms').addClass("active");
            $('.open-voice-sms').removeClass("active");
        });

        $('.open-voice-sms').click(function(){
            $('.send-sms').addClass("hide");
            $('.send-voice-sms').removeClass("hide");
            $('.open-voice-sms').addClass("active");
            $('.open-send-sms').removeClass("active");
        });

        $('.contact_2').addClass('hide');
        $('.open-contact-1').addClass("active");
        $('.open-contact-2').click(function(){
            $('.contact_1').addClass("hide");
            $('.contact_2').removeClass("hide");
            $('.open-contact-2').addClass("active");
            $('.open-contact-1').removeClass("active");
        });
        $('.open-contact-1').click(function(){
            $('.contact_2').addClass("hide");
            $('.contact_1').removeClass("hide");
            $('.open-contact-1').addClass("active");
            $('.open-contact-2').removeClass("active");
        });

        var limit = 100;
        $("#checkAll-1").click(function () {
            $('#mobile-numbers-contact-1 li :checkbox:lt('+limit+')').not(this).prop('checked', this.checked);
        });
        $("#checkAll-2").click(function () {
            $('#mobile-numbers-contact-2 li :checkbox:lt('+limit+')').not(this).prop('checked', this.checked);
        });
    });    
</script>
<script>
    $(".search-contact-1").keyup(filterAttractionsContact1);

    function filterAttractionsContact1() {
        var searchtext = $(".search-contact-1").val();
        $("#mobile-numbers-contact-1 li").each(function() {
            var attraction = $(this);
            var category = true;
            var text = attraction.text().indexOf(searchtext) === 0
            attraction.toggle(category && text);
        });
    }

    $(".search-contact-2").keyup(filterAttractionsContact2);

    function filterAttractionsContact2() {
        var searchtext = $(".search-contact-2").val();
        $("#mobile-numbers-contact-2 li").each(function() {
            var attraction = $(this);
            var category = true;
            var text = attraction.text().indexOf(searchtext) === 0
            attraction.toggle(category && text);
        });
    }

    var limit = 100;
    $('input[class= "numbers-cb-contact-1"]').click(function(){ 
        var id = this.id;
        var countCheckedCheckboxes = $('#mobile-numbers-contact-1 input[type="checkbox"]').filter(':checked').length;
        if(countCheckedCheckboxes <= limit) {
            if (this.checked)
            {   
                var span = "<label for='" + id + "' data-cb-id-1='" + id + "'>" + $(this).attr('rel') + "</label>";
                $(".checked-value-1").append(span);
            }
            else
            {
                $("label[data-cb-id-1='"+id+"']").remove();
            }
        }
    });

    $('input[class= "numbers-cb-contact-2"]').click(function(){ 
        var id = this.id;
        var countCheckedCheckboxes = $('#mobile-numbers-contact-2 input[type="checkbox"]').filter(':checked').length;
        if(countCheckedCheckboxes <= limit) {
            if (this.checked)
            {   
                var span = "<label for='" + id + "' data-cb-id-2='" + id + "'>" + $(this).attr('rel') + "</label>";
                $(".checked-value-2").append(span);
            }
            else
            {
                $("label[data-cb-id-2='"+id+"']").remove();
            }
        }
    });

    var selectAll_1 = [];
    $('input[id= "checkAll-1"]').on('change', function(evt){ 
        $('#mobile-numbers-contact-1 input[type="checkbox"]').each(function(){
            selectAll_1.push($(this));
        });
        var checkedLength = $('#mobile-numbers-contact-1 input[type="checkbox"]').filter(':checked').length;
        $('.checkbox-count').text("Checked: " + checkedLength);
        for(data in selectAll_1) {
            var id = selectAll_1[data].attr("id");
            if (selectAll_1[data].attr("checked") == 'checked')
            {   
                var span = "<label for='" + id + "' data-cb-id-1='" + id + "'>" + $(selectAll_1[data]).attr('rel') + "</label>";
                $(".checked-value-1").append(span);
            }
            else
            {
                $("label[data-cb-id-1='"+id+"']").remove();
            }

        }
        selectAll_1 = [];
    });

    var selectAll_2 = [];
    $('input[id= "checkAll-2"]').on('change', function(evt){ 
        $('#mobile-numbers-contact-2 input[type="checkbox"]').each(function(){
            selectAll_2.push($(this));
        });
        var checkedLength_2 = $('#mobile-numbers-contact-2 input[type="checkbox"]').filter(':checked').length;
        $('.checkbox-count').text("Checked: " + checkedLength_2);
        for(data in selectAll_2) {
            var id = selectAll_2[data].attr("id");
            if (selectAll_2[data].attr("checked") == 'checked')
            {   
                var span = "<label for='" + id + "' data-cb-id-2='" + id + "'>" + $(selectAll_2[data]).attr('rel') + "</label>";
                $(".checked-value-2").append(span);
            }
            else
            {
                $("label[data-cb-id-2='"+id+"']").remove();
            }
        }
        selectAll_2 = [];
    });
</script>
<script>
    function countChar(val) {
        var len = val.value.length;
        if (len >= 180) {
            val.value = val.value.substring(0, 180);
        } else {
            $('#charNum').text(180 - len + " characters remaining");
        }
    };

    var limit = 100;
    var checkboxes_1 = $('#mobile-numbers-contact-1 input[type="checkbox"]');
    checkboxes_1.on('change', function(evt) {
        var countCheckedCheckboxes = checkboxes_1.filter(':checked').length;
        if(countCheckedCheckboxes <= limit) {
            $('.checkbox-count').text("Checked: " + countCheckedCheckboxes);    
        }
        if(countCheckedCheckboxes > limit) {
            this.checked = false;
        }
    });

    var checkboxes_2 = $('#mobile-numbers-contact-2 input[type="checkbox"]');
    checkboxes_2.on('change', function(evt) {
        var countCheckedCheckboxes = checkboxes_2.filter(':checked').length;
        if(countCheckedCheckboxes <= limit) {
            $('.checkbox-count').text("Checked: " + countCheckedCheckboxes);
        }
        if(countCheckedCheckboxes > limit) {
            this.checked = false;
        }
    });
</script>
<script>
    function doconfirm(contact_type)
    {
        if(contact_type == 'contact_1') {
            job = confirm("Are you sure to delete permanently number\'s in Contact One?");
            if(job != true)
            {
                return false;
            } else {
                $('<input />').attr('type', 'hidden')
                    .attr('name', "contact_type")
                    .attr('value', "contact_1")
                    .appendTo('#send-form-1');
                $('<input />').attr('type', 'hidden')
                    .attr('name', "sms")
                    .attr('value', "delete_contact")
                    .appendTo('#send-form-1');         
                $('#send-form-1').submit();
                $('#loader').show();
            }   
        } else {
            job = confirm("Are you sure to delete permanently number\'s in Contact Two?");
            if(job != true)
            {
                return false;
            } else {
                $('<input />').attr('type', 'hidden')
                    .attr('name', "sms")
                    .attr('value', "delete_contact")
                    .appendTo('#send-form-2');  
                $('<input />').attr('type', 'hidden')
                    .attr('name', "contact_type")
                    .attr('value', "contact_2")
                    .appendTo('#send-form-2');     
                $('#send-form-2').submit();
                $('#loader').show();
            }   
        }    
    }
</script>