<!DOCTYPE html>
<html>              
<head>
	<title>Malaysian Timbers</title>
	<!--meta tag-->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url("media/css/bootstrap.min.css");?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url("media/css/bootstrap-responsive.css");?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url("media/css/style.css");?>" />
	<script type="text/javascript" src="<?php echo base_url("media/js/jquery-1.8.3.min.js");?>" /></script>
	<script type="text/javascript" src="<?php echo base_url("media/js/bootstrap.min.js");?>" /></script>
	<script type="text/javascript" src="<?php echo base_url("media/js/jquery.validate.min.js");?>" /></script>
</head>
<body>
	
