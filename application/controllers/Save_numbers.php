<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Save_numbers extends CI_Controller {
	public function index($status = "", $type = "", $mobile_numbers = "", $message = "")	{
		if($status == 'savenumbers') {
			if($mobile_numbers != "") {
				$decodeNumbers = base64_decode(urldecode($mobile_numbers));
				$data['mobileNumbers'] = $decodeNumbers;
				$data['mobile_Numbers'] = array();
				$data['Textmessage'] = "";
				$data['radioSelected'] = "";
			} else {
				$data['mobileNumbers'] = "";
				$data['mobile_Numbers'] = array();
				$data['Textmessage'] = "";
				$data['radioSelected'] = "";
			}
		} else {
			if($type == 'text') {
				if($mobile_numbers != "empty") {
					$decodeNumbers = base64_decode(urldecode($mobile_numbers));
					$data['mobile_Numbers'] = explode(",", $decodeNumbers);
				} else {
					$data['mobile_Numbers'] = array();
				}

				if($message != "empty") {
					$decodeMessage = base64_decode(urldecode($message));
					$data['Textmessage'] = $decodeMessage;
				} else {
					$data['Textmessage'] = "";
				}
			} else {
				if($mobile_numbers != "empty") {
					$decodeNumbers = base64_decode(urldecode($mobile_numbers));
					$data['mobile_Numbers'] = explode(",", $decodeNumbers);
				} else {
					$data['mobile_Numbers'] = array();
				}

				if($message != "empty") {
					$decodeMessage = base64_decode(urldecode($message));
					$data['radioSelected'] = $decodeMessage;
				} else {
					$data['radioSelected'] = "";
				}
			}
		}
		
		$SQL = "SELECT mobile_numbers.mobile_numbers, mobile_numbers.id, mobile_numbers.type, reports.datetime FROM mobile_numbers LEFT JOIN reports ON mobile_numbers.mobile_numbers = reports.mobile_number AND mobile_numbers.type = reports.type ORDER BY reports.datetime ASC";
		$records = $this->crud_model->get_by_sql($SQL);
		$contact1_Array = array();
		$contact2_Array = array();
		if(!empty($records)) {
			foreach ($records as $key => $value) {
				$currentDate = date("Y-m-d");
				if($value['type'] == 'contact_1') {
					$SQLDate = date("Y-m-d", strtotime($value['datetime']));
					if($SQLDate != $currentDate) {
						$contact1_Array[$value['mobile_numbers']] = $value;	
					}
				} else {
					$SQLDate = date("Y-m-d", strtotime($value['datetime']));
					if($SQLDate != $currentDate) {
						$contact2_Array[$value['mobile_numbers']] = $value;
					}
				}
			}
		}
		
		function date_compare($a, $b)
		{
		    $t1 = strtotime($a['datetime']);
		    $t2 = strtotime($b['datetime']);
		    return $t1 - $t2;
		}  
		usort($contact1_Array, 'date_compare');
		usort($contact2_Array, 'date_compare');
		$data['contact_1'] = $contact1_Array;
		$data['contact_2'] = $contact2_Array;

		//Get total Customers (Carpenter / Engineer)
		$getCountCarpenter_SQL = "SELECT mobile_numbers.mobile_numbers, mobile_numbers.id, mobile_numbers.type FROM mobile_numbers WHERE type = 'contact_1'";
		$getCountCarpenter_records = $this->crud_model->get_count_by_sql($getCountCarpenter_SQL);
		$data['getCountCarpenter'] = $getCountCarpenter_records;

		$getCountEngineer_SQL = "SELECT mobile_numbers.mobile_numbers, mobile_numbers.id, mobile_numbers.type FROM mobile_numbers WHERE type = 'contact_2'";
		$getCountEngineer_records = $this->crud_model->get_count_by_sql($getCountEngineer_SQL);
		$data['getCountEngineer'] = $getCountEngineer_records;

		//Get today sms sent (SMS / Voice)
		$getSMS_SQL = "SELECT * FROM reports WHERE sms_type = 'Text' AND DATE(reports.datetime) = DATE(NOW())";
		$getSMS_records = $this->crud_model->get_count_by_sql($getSMS_SQL);
		$data['getSMS'] = $getSMS_records;

		$getVoice_SQL = "SELECT * FROM reports WHERE sms_type = 'Voice' AND DATE(reports.datetime) = DATE(NOW())";
		$getVoice_records = $this->crud_model->get_count_by_sql($getVoice_SQL);
		$data['getVoice'] = $getVoice_records;
		
		//Get Voice Details
		$data['voiceFiles'] = $this->crud_model->get("voice_files");

		$reports = $this->crud_model->get("reports");
		if(!empty($reports)) {
			foreach ($reports as $reportsKey => $reportsValue) {
				if($reportsValue['sms_type'] == 'Voice') {
					$voiceCount[] = $reportsValue;
				} else {
					$TextCount[] = $reportsValue;
				}
			}
		}
		$data['voice_Count'] = 9997 - (isset($voiceCount)?count($voiceCount):0);
		$data['Text_Count'] = 10004 - (isset($TextCount)?count($TextCount):0);
		$this->load->view('templates/header');
		$this->load->view('save_numbers', $data);
		$this->load->view('templates/footer');
	}

	public function save_numbers_action() {
		if ($this->input->post("copy_paste") != "") 
		{
			$numbers = $this->input->post("numbers");
			$contactType = $this->input->post("contact_type");
			$numbers = str_replace ( ",", "\n", trim ( $numbers ) );
			$numbers = str_replace ( "\r", "", trim ( $numbers ) );
			$mobile_numbers = array_map ( 'trim', explode ( "\n", trim ( $numbers ) ) );
			$filter_mobile_numbers = array_filter($mobile_numbers);
			if (empty($filter_mobile_numbers)) 
			{
				$this->session->set_flashdata ( 'save_response', '<div class="alert alert-danger">Copy / Paste mobile number error</div>',':old:');
				redirect('save_numbers');
			}
			else
			{
				if (isset ( $filter_mobile_numbers ) and is_array ( $filter_mobile_numbers )) 
				{
					foreach ( $filter_mobile_numbers as $mobile_number ) 
					{
						$mobileNumbers = $this->clean(trim ( $mobile_number ));
						if(is_numeric($mobileNumbers) && strlen($mobileNumbers) == 10) {
							$data[] = array(
								'mobile_numbers' => $mobileNumbers,
								'type'	=> $contactType
							);
						}
					}
					if(!empty($data)) {
						$this->db->custom_insert_batch('mobile_numbers', $data); 
						$this->session->set_flashdata ( 'save_response', '<div class="alert alert-success">Mobile numbers added</div>',':old:');
						redirect('save_numbers');	
					} else {
						$this->session->set_flashdata ( 'save_response', '<div class="alert alert-danger">Copy / Paste mobile number error</div>',':old:');
						redirect('save_numbers/index/savenumbers/copy_paste/'.urlencode(base64_encode($numbers)));
					}					
				}
				else
				{
					$this->session->set_flashdata ( 'save_response', '<div class="alert alert-danger">No numbers entered</div>',':old:');
					redirect('save_numbers');
				}
			}	
		}
		elseif (isset($_FILES) and is_array($_FILES)) 
		{
			if($_FILES ["datafile"] ["name"] != "")
			{
				$contactType = $this->input->post("contact_type");
				$this->load->library('Spreadsheet_Excel_Reader');
				$barcodes = "";
				$file_name = pathinfo ( $_FILES ["datafile"] ["name"] );
				$ext = $file_name ['extension'];
				$prod_id_qty = "";				
				if ($ext == 'xls') 
				{
					if ($_FILES ["datafile"] ["error"] > 0) 
					{
						$file_error = "Error: " . $_FILES ["datafile"] ["error"] . "<br />";
						$this->session->set_flashdata ( 'save_response', '<div class="alert alert-danger">'.$file_error.'</div>',':old:' );
						redirect('save_numbers');
					} 
					else 
					{
						$upload_file_path = "uploads/";
						$upload_file_name = "upload" . rand ( 1, 1000 ) . "." . 'xls';
						move_uploaded_file ( $_FILES ["datafile"] ["tmp_name"], $upload_file_path . $upload_file_name );
						$newxls = $upload_file_path . $upload_file_name;
						$data = new Spreadsheet_Excel_Reader ( $newxls );
						if($data->error === true)
						{
							$this->session->set_flashdata ( 'save_response', '<div class="alert alert-danger">Uploaded file is not recognized as .xls format. Please try converting this file to .xls or .csv and try again.</div>',':old:' );
							redirect('save_numbers');
						}
						else
						{
							error_reporting ( E_ALL ^ E_NOTICE );
							$noofrows = $data->rowcount ( 0 );
							$noofcols = $data->colcount ( 0 );
							$prod_id_qty = "";
							if($noofcols != "")
							{
								if ($noofcols >= 1) 
								{
									for($i = 1; $i <= $noofrows; $i ++) 
									{
										$numbers = $data->val ( $i, 1, 0 );
										$mobileNumbers = $this->clean(trim ( $mobile_number ));
										if(is_numeric($mobileNumbers) && strlen($mobileNumbers) == 10) {
											if($mobileNumbers != "") {
												$insertData[] = array(
													'mobile_numbers' => $mobileNumbers,
													'type'	=> $contactType
												);
											}
										}
									}
									$this->db->custom_insert_batch('mobile_numbers', $insertData);
									unlink ( $newxls );
									$this->session->set_flashdata ( 'save_response', '<div class="alert alert-success">Mobile numbers saved</div>',':old:' );
									redirect("save_numbers");
								} 
								else 
								{
									unlink ( $newxls );
									$this->session->set_flashdata ( 'save_response', '<div class="alert alert-danger">The file must have one colums</div>',':old:' );
									redirect('save_numbers');
								}
							}
							else
							{
								unlink ( $newxls );
								$this->session->set_flashdata ( 'save_response', '<div class="alert alert-danger">File empty</div>',':old:' );
								redirect('save_numbers');
							}
						}
					}
				} 
				if ($ext == 'csv') 
				{
					if ($_FILES ["datafile"] ["error"] > 0) 
					{
						$file_error = "Error: " . $_FILES ["datafile"] ["error"] . "<br />";
						$this->session->set_flashdata ( 'save_response', '<div class="alert alert-danger">'.$file_error.'</div>',':old:' );
						redirect('save_numbers');
					}
					else 
					{
						$upload_file_path = "uploads/";
						$upload_file_name = "upload" . rand ( 1, 1000 ) . "." . 'csv';
						move_uploaded_file ( $_FILES ["datafile"] ["tmp_name"], $upload_file_path . $upload_file_name );
						$newcsv = $upload_file_path . $upload_file_name;
						$count = 0;
						$fp = fopen ( $newcsv, 'r' ) or die ( "can't open file" );
						$csv_line = fgetcsv ( $fp, 1024 );
						echo "<pre>";
						if(!empty($csv_line))
						{
							do 
							{
								$csv_line[0] = trim(isset($csv_line[0])?$csv_line[0]:"");
								if($csv_line[0]!="")
								{
									$numbers = $csv_line[0];
									$mobileNumbers = $this->clean(trim ( $mobile_number ));
									if(is_numeric($mobileNumbers) && strlen($mobileNumbers) == 10) {
										$getProductPN[$mobileNumbers] = $mobileNumbers;
									}
								}
							} while ($csv_line = fgetcsv ( $fp, 1024 ));
							
							if(!empty($getProductPN))
							{
								foreach ($getProductPN as $key1 => $value1) {
									$data[] = array(
										'mobile_numbers' => $value1,
										'type'	=> $contactType
									);
								}
								fclose ( $fp ); // or die("can't close file");
								unlink ( $newcsv );
								$this->db->custom_insert_batch('mobile_numbers', $data);
								$this->session->set_flashdata ( 'save_response', '<div class="alert alert-success">Mobile numbers saved</div>',':old:' );
								redirect('save_numbers');
							}
							else
							{
								fclose ( $fp ); // or die("can't close file");
								unlink ( $newcsv );
								$this->session->set_flashdata ( 'save_response', '<div class="alert alert-danger">The file must have one colums</div>',':old:' );
								redirect('save_numbers');
							}
						}
						else
						{
							fclose ( $fp ); // or die("can't close file");
							unlink ( $newcsv );
							$this->session->set_flashdata ( 'save_response', '<div class="alert alert-danger">File empty</div>',':old:');
							redirect('save_numbers');
						}
					}
				} 
				else 
				{
					$this->session->set_flashdata ( 'save_response', '<div class="alert alert-danger">Invalid file format. Please upload only .csv or .xls file format. If you have .xslx file, save it as .xls format and try uploading again</div>',':old:' );
					redirect('save_numbers');
				}
			}
			else
			{
				$this->session->set_flashdata ( 'save_response', '<div class="alert alert-danger">Please Select File</div>',':old:' );
				redirect('save_numbers');
			}
		}
	}

	public function clean($string) {
	   $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
	   return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
	}

	public function send_SMS()
	{
		if($this->input->post('sms') == 'send voice sms') {
			$mobileNumbers = $this->input->post('selected_numbers');
			$type = $this->input->post('type');
			$explodeNumbers = explode(",", $mobileNumbers);
			$fileID = $this->input->post('voice_file');
			if($mobileNumbers != "" && $fileID != "") {
				foreach ($explodeNumbers as $key => $value) {
					$url = "http://call.smshorizon.in/api/v1/index.php?api_key=Ab45203b2c5d702fb6259da608799ac38&method=voice.call&play=".$fileID.".sound&numbers=".$value."&format=txt";
					$ch = curl_init($url);
					curl_setopt($ch, CURLOPT_URL, $url);
					curl_setopt($ch, CURLOPT_TIMEOUT, 500);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

					$result = curl_exec($ch);
					if (curl_errno($ch)) {
					    log_message('error', curl_error($ch)."\n\n");
					}
					curl_close ($ch);

					$results = json_decode($result);
					$data = array(
						'mobile_number' => $value,
						'type'			=> $type,
						'sms_type'		=> 'Voice',
						'status'		=> $results->status,
						'message'		=> $results->message,
						'message_id'	=> $results->data->id,
						'datetime'		=> date("Y-m-d H:i:s")
					);
					$this->db->insert('reports', $data);
				}
				$this->session->set_flashdata ( 'send_response', '<div class="alert alert-success">Voice call sent successfully</div>' );
				redirect('save_numbers');
			} else {
				if($mobileNumbers == "" && $fileID == "") {
					$this->session->set_flashdata ( 'send_response', '<div class="alert alert-danger">Please select mobile number\'s and select your voice file</div>' );
					$Mobile_Number = "empty";
					$messageText = "empty";
				}
				else {
					if($mobileNumbers != "") {
						$Mobile_Number = urlencode(base64_encode($mobileNumbers));
					} else {
						$this->session->set_flashdata ( 'send_response', '<div class="alert alert-danger">Please select mobile number\'s</div>' );
						$Mobile_Number = "empty";
					}
					if($fileID != "") {
						$selectedFile = urlencode(base64_encode($fileID));
					} else {
						$this->session->set_flashdata ( 'send_response', '<div class="alert alert-danger">Please select your voice file</div>' );
						$selectedFile = "empty";
					}
				}
				redirect('save_numbers/index/sendsms/voice/'.$Mobile_Number.'/'.$selectedFile);
			}
		} elseif($this->input->post('sms') == 'send text sms') {
			$mobileNumbers = $this->input->post('selected_numbers');
			$type = $this->input->post('type');
			$explodeNumbers = explode(",", $mobileNumbers);
			$textMessage = $this->input->post('send_sms');
			if($mobileNumbers != "" && $textMessage != "") {
				foreach ($explodeNumbers as $key => $value) {
					$url = "http://smshorizon.co.in/api/sendsms.php?user=mansur&apikey=gAfZgKgApZ4XFgi8Wqoj&mobile=".$value."&message=".urlencode($textMessage)."&senderid=MALAYS&type=txt";
					
					$ch = curl_init($url);
					curl_setopt($ch, CURLOPT_URL, $url);
					curl_setopt($ch, CURLOPT_TIMEOUT, 500);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

					$result = curl_exec($ch);
					if (curl_errno($ch)) {
					    log_message('error', curl_error($ch)."\n\n");
					}
					curl_close ($ch);

					$results = json_decode($result);
					$data = array(
						'mobile_number' => $value,
						'type'			=> $type,
						'sms_type'		=> 'Text',
						'status'		=> '',
						'message'		=> '',
						'message_id'	=> $result,
						'datetime'		=> date("Y-m-d H:i:s")
					);
					$this->db->insert('reports', $data);

				}
				$this->session->set_flashdata ( 'send_response', '<div class="alert alert-success">Your message sent successfully...</div>' );
				redirect('save_numbers');
			} else {
				if($mobileNumbers == "" && $textMessage == "") {
					$this->session->set_flashdata ( 'send_response', '<div class="alert alert-danger">Please select mobile number\'s and type your message</div>' );
					$Mobile_Number = "empty";
					$messageText = "empty";
				}
				else {
					if($mobileNumbers != "") {
						$Mobile_Number = urlencode(base64_encode($mobileNumbers));
					} else {
						$this->session->set_flashdata ( 'send_response', '<div class="alert alert-danger">Please select mobile number\'s</div>' );
						$Mobile_Number = "empty";
					}
					if($textMessage != "") {
						$messageText = urlencode(base64_encode($textMessage));
					} else {
						$this->session->set_flashdata ( 'send_response', '<div class="alert alert-danger">Please type your message</div>' );
						$messageText = "empty";
					}
				}
				redirect('save_numbers/index/sendsms/text/'.$Mobile_Number.'/'.$messageText);
			}
		} elseif($this->input->post('sms') == 'delete_contact') {
			$deletedNumbers = $this->input->post('mobile_numbers');
			$contactType = $this->input->post('contact_type');
			if(!empty($deletedNumbers)) {
				$this->db->where_in('mobile_numbers', $deletedNumbers);
				$this->db->where('type', $contactType);
				$this->db->delete('mobile_numbers');
				$this->session->set_flashdata ( 'send_response', '<div class="alert alert-success">Delete successfully...</div>' );
				redirect('save_numbers');	
			} else {
				$this->session->set_flashdata ( 'send_response', '<div class="alert alert-danger">Please select mobile numbers...</div>' );
				redirect('save_numbers');
			}
		}
	}
}
